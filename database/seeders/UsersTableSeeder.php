<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('users')->delete();
        
        \DB::table('users')->insert(array (
            0 => 
            array (
                'id' => 1,
                'name' => 'Kokleng',
                'username' => 'admin',
                'password' => '12345678',
                'email' => 'gmail@gmail.com',
                'email_verified_at' => NULL,
                'remember_token' => NULL,
                'created_by' => NULL,
                'updated_by' => NULL,
                'created_at' => '2023-01-02 18:20:47',
                'updated_at' => '2023-01-04 20:07:53',
                'active' => 1,
                'photo' => 'image/L4HxmUhZ2hBAUWqdriNpwGBnBkqep2lVt2gbd0Zi.jpg',
            ),
        ));
        
        
    }
}