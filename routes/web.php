<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\backend\HomeController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['prefix' => 'admin'], function(){
    Route::get('/login', 'backend\AuthController@login')->name('auth.login');
    Route::post('/login', 'backend\AuthController@login')->name('auth.login');
    Route::get('/logout', 'backend\AuthController@logout')->name('auth.logout');

    Route::middleware(['checkAuth'])->group(function () {
        Route::get('/', 'backend\HomeController@index')->name('dashboard');

        // company info
        Route::get('/company', 'backend\CompanyController@index')->name('admin.company');
        Route::post('/company/edit', 'backend\CompanyController@edit')->name('admin.company.edit');

        //my profile
        Route::get('/my-profile', 'backend\ProfileController@index')->name('admin.profile');
        Route::post('/update-profile', 'backend\ProfileController@update')->name('admin.profile.update');

        //user
        Route::get('/user', 'backend\UserController@index')->name('admin.user');
        Route::get('/user/edit/{id}', 'backend\UserController@edit')->name('admin.edit');
        Route::post('/user/update/{id}', 'backend\UserController@update')->name('admin.update');
        Route::get('/user/create', 'backend\UserController@create')->name('admin.create');
        Route::get('/user/delete', 'backend\UserController@delete')->name('admin.delete');

        //datatable
        Route::post('/bulk/insert', 'backend\DatatableController@insert')->name('admin.datatable.insert');
        Route::get('/bulk/getDetail/{id}/{tbl}', 'backend\DatatableController@edit')->name('admin.datatable.edit');
        Route::post('/bulk/update', 'backend\DatatableController@update')->name('admin.datatable.update');
        Route::get('/bulk/delete', 'backend\DatatableController@delete')->name('admin.datatable.delete');
        Route::get('/bulk/deleteAll', 'backend\DatatableController@deleteAll')->name('admin.datatable.deleteAll');


         


        //category
        Route::get('/category', 'backend\CategoryController@index')->name('admin.category');

        //payment method
        Route::get('/payment-method', 'backend\PaymentMethodController@index')->name('admin.payment_method');

        // videos
        Route::get('/video', 'backend\VideoController@index')->name('admin.video');

        // banner
        Route::get('/banner', 'backend\BannerController@index')->name('admin.banner');

        // product
        Route::get('/product', 'backend\ProductController@index')->name('admin.product');

    });

    
});

// switch language
Route::get('/switch-lange/{lang}', 'LanguageController@switchLanguage')->name('app.lang');

//front end
Route::get('/', function(){
    return view('frontend.Home.index');
});


Route::fallback(function(){
    return redirect()->route('dashboard');
});




