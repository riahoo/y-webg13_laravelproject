<?php

function user($con){
    $u = DB::table('users')->find(session()->get('user')->id);
    
    $v = '';
    switch ($con) {
        case "id":
            $v = $u->id;
            break;
        case "username":
            $v = $u->username;
            break;
        case "photo":
            $v = $u->photo;
            break;
        case "name":
            $v = $u->name;
            break;
        default:
            $v = '';
    };

    return $v;
}

function company($con){
    $com = DB::table("companies")->find(1);
    $v = '';
    switch ($con) {
        case "name":
            $v = $com->name;
            break;
        case "photo":
            $v = $com->photo;
            break;
        default:
            $v = '';
    };

    return $v;
}