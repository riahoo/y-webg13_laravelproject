<?php

namespace App\Http\Controllers\backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App;

class HomeController extends Controller
{
    public function index(){
        // if(!session()->has('user')){
        //     return redirect()->route('auth.login');
        // }
        return view('backend.home.index');
    }
    public function test(){
        session()->put('username','chenda');
        return redirect()
                ->route('dashboard')
                ->with('success','this is session!');
    }
}
