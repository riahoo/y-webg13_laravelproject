<?php

namespace App\Http\Controllers\backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;

class CompanyController extends Controller
{
    public function index(){
        $data['company'] = DB::table("companies")->find(1);
        return view('backend.companies.index',$data);
    }
    public function edit(Request $r){
        $data = $r->except('_token','photo');
        if($r->photo){
            $data['photo'] = $r->photo->store('/image','custom');
        }
        $update = DB::table('companies')->where('id',1)->update($data);
        if($update){
            return redirect()->back()->with('success','update successfully!');
        } else {
            return redirect()->back()->with('error','update fails!');
        }
    }
}
