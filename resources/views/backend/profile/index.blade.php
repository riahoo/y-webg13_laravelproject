@extends('backend.layouts.master')
@section('title')
    home page
@endsection
@section('content-title')
   <h2> Company Infomation</h2>
@endsection
@section('content')
@include('backend.utils.alert')
    <div class="row">
        <div class="col">
            <div class="card">
                <div class="card-body">
                    <div class="row">
                        <div class="col-12 text-right">
                            <button data-toggle="modal" data-target="#edit"  type="button" class="btn btn-outline-success">Edit</button>
                        </div>
                        <div class="col-12">
                            <div class="text-center w-100 my-5">
                                <img src="{{ asset($user->photo ? $user->photo : 'https://t3.ftcdn.net/jpg/04/34/72/82/360_F_434728286_OWQQvAFoXZLdGHlObozsolNeuSxhpr84.jpg') }}" class="rounded-circle shadow" width="150" height="150">
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-12">
                            <div class="row">
                                <div class="col-5">
                                    <label for="name">Name</label>
                                </div>
                                <div class="col-1"> : </div>
                                <div class="col">
                                    <span>{{ $user->name }}</span>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-12">
                            <div class="row">
                                <div class="col-6">
                                    <label for="name">Username</label>
                                </div>
                                <div class="col-1"> : </div>
                                <div class="col">
                                    <span>{{ $user->username }}</span>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-12">
                            <div class="row">
                                <div class="col-5">
                                    <label for="name">Email</label>
                                </div>
                                <div class="col-1"> : </div>
                                <div class="col">
                                    <span>{{ $user->email }}</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @include('backend.profile.edit')
@endsection
