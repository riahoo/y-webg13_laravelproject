<!-- Navbar -->
<nav class="main-header navbar navbar-expand navbar-white navbar-light">
    <!-- Left navbar links -->
    <ul class="navbar-nav">
      <li class="nav-item">
        <a class="nav-link" data-widget="pushmenu" href="#" role="button"><i class="fas fa-bars"></i></a>
      </li>
    </ul>

    <!-- Right navbar links -->
    <ul class="navbar-nav ml-auto">
      <li class="nav-item">
        <a class="nav-link" data-widget="fullscreen" href="#" role="button">
          <i class="fas fa-expand-arrows-alt"></i>
        </a>
      </li>
      <div class="dropdown">
        <button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown" aria-expanded="false">
          Profile
        </button>
        <div class="dropdown-menu">
          <a class="dropdown-item" href="#">
            <img src="{{ asset(user("photo")) }}" width="24" height="24" class="rounded-circle" alt="">
            My Profile
          </a>
          <a class="dropdown-item" href="{{ route("app.lang", "en") }}">
            <img src="{{ asset('en.png') }}" width="20"> &nbsp; English
          </a>
          <a class="dropdown-item" href="{{ route("app.lang", "km") }}">
            <img src="{{ asset('km.png') }}" width="24"> &nbsp; Khmer
          </a>
          <a class="dropdown-item" href="{{ route('auth.logout') }}">
            <i class="fas fa-sign-out-alt mr-2"></i> Logout
          </a>
        </div>
      </div>
    </ul>
  </nav>