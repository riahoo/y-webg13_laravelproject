@if(session('success') || session('error'))
<div class="row">
    <div class="col-12">
        @if (session()->has('success'))
            <div class="alert alert-success">
                {{ session()->get('success') }}
            </div>
        @else
            <div class="alert alert-error">
                {{ session()->get('error') }}
            </div>
        @endif
    </div>
</div>
@endif